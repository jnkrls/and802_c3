package c3_1.seguridad.and802.c3_1

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration

class AndroidApplication: Application(){
    override fun onCreate() {
        super.onCreate()

        Realm.init(this)
        val realCfg = RealmConfiguration.Builder()
            .name("clase.realm")
            .schemaVersion(1)
            .build()

        Realm.setDefaultConfiguration(realCfg)
    }
}