package c3_1.seguridad.and802.c3_1

import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        val AUTORIZACION    = "com.google.clase02_3.provider"
        val URL             = Uri.parse("content://$AUTORIZACION/t1")

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()

        btnLeer.setOnClickListener {

            val cursor = contentResolver
                .query(URL, null, null,null, null)


            if(cursor.moveToFirst()){
                while(cursor.moveToNext()){
                    val nombre = cursor.getString(cursor.getColumnIndex("nombre"))
                    val apellido = cursor.getString(cursor.getColumnIndex("apellido"))
                    val correo = cursor.getString(cursor.getColumnIndex("correo"))
                    val numero = cursor.getString(cursor.getColumnIndex("numero"))

                    Log.d("MENSAJE", "$nombre, $apellido, $correo, $numero")
                }
            }

        }
    }

}
