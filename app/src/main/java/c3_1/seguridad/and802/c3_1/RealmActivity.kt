package c3_1.seguridad.and802.c3_1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import c3_1.seguridad.and802.c3_1.database.PersonaImpl
import c3_1.seguridad.and802.c3_1.entity.Persona
import kotlinx.android.synthetic.main.activity_realm.*

class RealmActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_realm)
    }

    override fun onResume() {
        super.onResume()

        btnGuardar.setOnClickListener {
            val nombre = etNombre.text.toString()
            val apellido = etApellido.text.toString()
            val correo = etCorreo.text.toString()
            val numero = etNumero.text.toString()

            val persona = Persona()
            persona.nombre = nombre
            persona.apellido = apellido
            persona.correo = correo
            persona.numero = numero

            val bd = PersonaImpl()
            bd.guardar(persona)

            Toast.makeText(this, "Se guardo satisfactoriamente", Toast.LENGTH_SHORT).show()

        }

        btnListar.setOnClickListener {
            val bd = PersonaImpl()
            val datos = bd.listar()

            var resultado = ""

            for(item: Persona in datos){
                resultado += "${item.id} - ${item.nombre} - ${item.apellido} - ${item.correo} - ${item.numero}\n"
            }

            tvDatos.text = resultado
        }
    }
}
