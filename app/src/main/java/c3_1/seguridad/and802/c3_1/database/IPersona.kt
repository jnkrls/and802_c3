package c3_1.seguridad.and802.c3_1.database

import c3_1.seguridad.and802.c3_1.entity.Persona
import io.realm.RealmResults

interface IPersona{
    fun guardar(persona: Persona):Persona
    fun listar():RealmResults<Persona>

}