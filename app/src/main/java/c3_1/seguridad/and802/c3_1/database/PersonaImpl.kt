package c3_1.seguridad.and802.c3_1.database

import c3_1.seguridad.and802.c3_1.entity.Persona
import io.realm.Realm
import io.realm.RealmResults
import java.util.*

class PersonaImpl:IPersona{

    override fun guardar(persona: Persona): Persona {
        val realm= Realm.getDefaultInstance()

        realm.beginTransaction()

        persona.id = UUID.randomUUID().toString()

        val resultado = realm.copyToRealmOrUpdate(persona)
        realm.commitTransaction()

        return resultado
    }

    override fun listar(): RealmResults<Persona> {
        val realm=Realm.getDefaultInstance()

        return realm.where(Persona::class.java).findAll()
    }

}