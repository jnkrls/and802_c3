package c3_1.seguridad.and802.c3_1.entity

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Persona:RealmObject(){
    @PrimaryKey
    var id:String=""
    var nombre:String=""
    var apellido:String=""
    var correo:String=""
    var numero:String=""

}